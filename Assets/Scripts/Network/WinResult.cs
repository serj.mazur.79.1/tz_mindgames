using Unity.Netcode;
using UnityEngine;

public class WinResult : NetworkBehaviour
{
    [SerializeField] private GameObject WinText;

    [ClientRpc]
    public void SetTextClientRpc(bool param)
    {
        WinText.SetActive(param);
    }
}
