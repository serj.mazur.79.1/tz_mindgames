using Unity.Netcode;
using UnityEngine;
using UnityEngine.Events;

public class FinalCubeHolder : NetworkBehaviour
{
    [HideInInspector] public UnityEvent<bool> PutTakeEvent;
    public bool insideCube { get; private set; }
    [SerializeField] private GameObject putUI;
    [SerializeField] private Transform holdingPosition;
    private TransferCube cubeInside;

    public override void OnDestroy()
    {
        if (cubeInside)
        {
            cubeInside.TookEvent.RemoveListener(TookFromHolder);
        }
    }

    public void PutCubeInHolder(NetworkObject cube)
    {
        if (IsServer)
        {
            var obj = NetworkManager.Singleton.SpawnManager.InstantiateAndSpawn(cube, NetworkManager.ServerClientId,
                   false, false, false, holdingPosition.position, holdingPosition.rotation);
            if(obj.TryGetComponent<TransferCube>(out TransferCube transferCube))
            {
                cubeInside = transferCube;
                insideCube = true;
                cubeInside.TookEvent.AddListener(TookFromHolder);
                PutTakeEvent?.Invoke(true);
            }

        }
    }

    private void TookFromHolder()
    {
        cubeInside.TookEvent.RemoveListener(TookFromHolder);
        cubeInside = null;
        insideCube = false;
        PutTakeEvent?.Invoke(false);
    }

    [ClientRpc]
    public void SetNotificationClientRpc(bool param, ulong clientId)
    {
        if (NetworkManager.LocalClientId == clientId)
        {
            putUI.SetActive(param);
        }
    }
}
