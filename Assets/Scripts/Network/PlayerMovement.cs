using UnityEngine;
using Unity.Netcode;

public class PlayerMovement : NetworkBehaviour
{
    [SerializeField] private GameObject targetCamera;
    [SerializeField] private float force = 200f;
    [SerializeField] private Rigidbody rb;
    [SerializeField] private Vector2 spawnOffset = new Vector2(5, 5);
    private NetworkVariable<Vector3> vectorMovement = new NetworkVariable<Vector3>();
    private Vector3 input;

    private void Start()
    {
        if (!IsOwner)
        {
            Destroy(targetCamera);
        }
        if (IsServer && IsOwner == false)
        {
            transform.position += new Vector3(Random.Range(-spawnOffset.x, spawnOffset.x), 0, Random.Range(-spawnOffset.y, spawnOffset.y));
        }
    }

    private void FixedUpdate()
    {
        if (IsServer)
        {
            if (IsOwner)
            {
                input = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
                input.x = Input.GetAxis("Horizontal");
                input.z = Input.GetAxis("Vertical");
                input *= force * Time.fixedDeltaTime;
                rb.velocity = input;
            }
            else
            {
                input = vectorMovement.Value;
                input *= force * Time.fixedDeltaTime;
                rb.velocity = input;
            }
        }
        else
        {
            input = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            if (vectorMovement.Value != input)
            {
                UpdateMovementServerRPC(input);
            }
        }
    }

    [ServerRpc]
    private void UpdateMovementServerRPC(Vector3 param)
    {
        vectorMovement.Value = param;
    }
}
