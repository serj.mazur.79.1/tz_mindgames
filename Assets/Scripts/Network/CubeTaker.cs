using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class CubeTaker : NetworkBehaviour
{
    [SerializeField] private GameObject tookUI;
    [SerializeField] private NetworkObject transferCube;
    [SerializeField] private Transform dropPointCube;
    private List<TransferCube> cubes = new List<TransferCube>();
    private List<FinalCubeHolder> holders = new List<FinalCubeHolder>();
    private bool tookCube;
    private TransferCube closestCube;
    private FinalCubeHolder closestHolder;

    private void Update()
    {
        if (IsOwner)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                TakeDropCubeServerRpc();
            }
        }
    }

    private void FixedUpdate()
    {
        if (IsServer)
        {
            cubes.Clear();
            holders.Clear();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (IsServer)
        {
            if (other.TryGetComponent<TransferCube>(out TransferCube cube))
            {
                cubes.Add(cube);
            }
            else if (other.TryGetComponent<FinalCubeHolder>(out FinalCubeHolder holder))
            {
                holders.Add(holder);
            }
            CheckClosestCube();
            CheckClosestHolder();
        }
    }   

    private void CheckClosestCube()
    {
        if (tookCube == false)
        {
            if (cubes.Count > 0)
            {
                float minDis = float.MaxValue;
                int id = 0;
                for (int i = 0; i < cubes.Count; i++)
                {
                    float dis = Vector3.Distance(transform.position, cubes[i].transform.position);
                    if (dis < minDis)
                    {
                        minDis = dis;
                        id = i;
                    }
                }
                if (closestCube != null)
                {
                    if (closestCube != cubes[id])
                    {
                        closestCube.SetNotificationClientRpc(false, OwnerClientId);
                        closestCube = cubes[id];
                        closestCube.SetNotificationClientRpc(true, OwnerClientId);
                    }
                }
                else
                {
                    closestCube = cubes[id];
                    closestCube.SetNotificationClientRpc(true, OwnerClientId);
                }
            }
            else
            {
                if (closestCube != null)
                {
                    closestCube.SetNotificationClientRpc(false, OwnerClientId);
                    closestCube = null;
                }
            }
        }
        else
        {
            if(closestCube != null)
            {
                closestCube.SetNotificationClientRpc(false, OwnerClientId);
                closestCube = null;
            }
        }
    }

    private void CheckClosestHolder()
    {
        if (tookCube)
        {
            if (holders.Count > 0)
            {
                float minDis = float.MaxValue;
                int id = -1;
                for (int i = 0; i < holders.Count; i++)
                {
                    if (holders[i].insideCube == false)
                    {
                        float dis = Vector3.Distance(transform.position, holders[i].transform.position);
                        if (dis < minDis)
                        {
                            minDis = dis;
                            id = i;
                        }
                    }
                }
                if (id != -1)
                {
                    if (closestHolder != null)
                    {
                        if (closestHolder != holders[id])
                        {
                            closestHolder.SetNotificationClientRpc(false, OwnerClientId);
                            closestHolder = holders[id];
                            closestHolder.SetNotificationClientRpc(true, OwnerClientId);
                        }
                    }
                    else
                    {
                        closestHolder = holders[id];
                        closestHolder.SetNotificationClientRpc(true, OwnerClientId);
                    }
                }
                else
                {
                    if (closestHolder != null)
                    {
                        closestHolder.SetNotificationClientRpc(false, OwnerClientId);
                        closestHolder = null;
                    }
                }
            }
            else
            {
                if (closestHolder != null)
                {
                    closestHolder.SetNotificationClientRpc(false, OwnerClientId);
                    closestHolder = null;
                }
            }
        }
        else
        {
            if (closestHolder != null)
            {
                closestHolder.SetNotificationClientRpc(false, OwnerClientId);
                closestHolder = null;
            }
        }
    }

    [ServerRpc]
    private void TakeDropCubeServerRpc()
    {
        if (tookCube) 
        {
            tookCube = false;
            if (closestHolder && closestHolder.insideCube == false)
            {
                closestHolder.PutCubeInHolder(transferCube);
            }
            else
            {
                NetworkManager.Singleton.SpawnManager.InstantiateAndSpawn(transferCube, NetworkManager.ServerClientId,
                       false, false, false, dropPointCube.position, dropPointCube.rotation);
            }
            SetActiveTookUIClientRpc(false);

        }
        else
        {
            if (closestCube != null)
            {
                tookCube = true;
                closestCube.TakeCube();
                SetActiveTookUIClientRpc(true);
            }
        }
    }

    [ClientRpc]
    private void SetActiveTookUIClientRpc(bool param)
    {
        tookUI.SetActive(param);
    }
}
