using Unity.Netcode;
using UnityEngine;
using UnityEngine.Events;

public class TransferCube : NetworkBehaviour
{
    [HideInInspector] public UnityEvent TookEvent;
    [SerializeField] private GameObject takeUI;
    
    public void TakeCube()
    {
        if (IsServer)
        {
            TookEvent?.Invoke();
            Destroy(gameObject);
        }
    }

    [ClientRpc]
    public void SetNotificationClientRpc(bool param, ulong clientId)
    {
        if (NetworkManager.LocalClientId == clientId)
        {
            takeUI.SetActive(param);
        }
    }
}
