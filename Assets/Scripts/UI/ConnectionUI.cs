using System.Net.Sockets;
using System.Net;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Unity.Netcode;
using Unity.Netcode.Transports.UTP;

public class ConnectionUI : MonoBehaviour
{
    [SerializeField] private UnityTransport unityTransport;
    [SerializeField] private GameObject uiForConnection;
    [SerializeField] private TMP_InputField inputFieldIP;
    [SerializeField] private TextMeshProUGUI ipText;
    [SerializeField] private Button hostButton;
    [SerializeField] private Button clientButton;
    private string ip;

    private void Start()
    {
        ip = GetIP();
        inputFieldIP.text = ip;
        ipText.text = ip;
        hostButton.onClick.AddListener(StartHost);
        clientButton.onClick.AddListener(StartClient);
    }

    private void OnDestroy()
    {
        hostButton.onClick.RemoveAllListeners();
        clientButton.onClick.RemoveAllListeners();
    }

    private void StartHost()
    {
        unityTransport.ConnectionData.ServerListenAddress = ip;
        NetworkManager.Singleton.StartHost();
        uiForConnection.SetActive(false);
    }

    private void StartClient()
    {
        unityTransport.ConnectionData.Address = inputFieldIP.text;
        NetworkManager.Singleton.StartClient();
        uiForConnection.SetActive(false);
        
    }

    private string GetIP()
    {
        var host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (var ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                return ip.ToString();
            }
        }
        return string.Empty;
    }
}
