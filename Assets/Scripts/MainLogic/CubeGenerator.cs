using System;
using Unity.Netcode;
using UnityEngine;

public class CubeGenerator : MonoBehaviour
{
    [SerializeField] private WinResult winResult;
    [SerializeField] private NetworkObject exampleCube;
    [SerializeField] private NetworkObject transferCube;
    [SerializeField] private SpawnPairs[] spawnPairs;
    private bool isServer;

    private void Start()
    {
        NetworkManager.Singleton.OnServerStarted += GenerateCubes;
    }

    private void OnDestroy()
    {
        if (isServer)
        {
            for (int i = 0; i < spawnPairs.Length; i++)
            {
                spawnPairs[i].finalCubeHolder.PutTakeEvent.RemoveListener(CheckRight);
            }
        }
    }

    private void GenerateCubes()
    {
        NetworkManager.Singleton.OnServerStarted -= GenerateCubes;
        isServer = true;
        for (int i = 0; i < spawnPairs.Length; i++)
        {
            if (UnityEngine.Random.Range(0, 3) > 0)
            {
                NetworkManager.Singleton.SpawnManager.InstantiateAndSpawn(exampleCube, NetworkManager.ServerClientId,
                    false, false, false, spawnPairs[i].exampleSpawnPoint.position, spawnPairs[i].exampleSpawnPoint.rotation);
                NetworkManager.Singleton.SpawnManager.InstantiateAndSpawn(transferCube, NetworkManager.ServerClientId,
                    false, false, false, spawnPairs[i].transferSpawnPoint.position, spawnPairs[i].transferSpawnPoint.rotation);
                spawnPairs[i].generated = true;
            }
            spawnPairs[i].finalCubeHolder.PutTakeEvent.AddListener(CheckRight);
        }
    }

    private void CheckRight(bool param)
    {
        for (int i = 0; i < spawnPairs.Length; i++)
        {
            if (spawnPairs[i].generated != spawnPairs[i].finalCubeHolder.insideCube)
            {
                EndGame(false);
                return;
            }
        }
        EndGame(true);
    }

    private void EndGame(bool param)
    {
        winResult.SetTextClientRpc(param);
    }

    [Serializable]
    public class SpawnPairs
    {
        public FinalCubeHolder finalCubeHolder;
        public Transform exampleSpawnPoint;
        public Transform transferSpawnPoint;
        public bool generated;
    }
}


